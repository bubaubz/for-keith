# Ass 1

Team members:
* Steve Shilling

## References and googling

### Docker-compose

* GOOGLE: docker-compose.yaml multiple dockerfiles
  * RESULT: https://stackoverflow.com/questions/29835905/docker-compose-using-multiple-dockerfiles-for-multiple-services  =  dockerfile
* GOOGLE: docker-compose.yaml name image
  * RESULT: https://stackoverflow.com/questions/32230577/how-do-i-define-the-name-of-image-built-with-docker-compose  =  image
* GOOGLE: docker-compose.yaml name container
  * RESULT: https://stackoverflow.com/questions/32230577/how-do-i-define-the-name-of-image-built-with-docker-compose  =  container_name
